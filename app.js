const express = require('express');
const bodyParser = require('body-parser');
const app = express();
require('dotenv').config()

// parse application/json
app.use(bodyParser.json());

// define routes
const ResourceRoutes = require('./src/routes/resourceRoutes');
app.use('/resources', ResourceRoutes);

// start the server
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});