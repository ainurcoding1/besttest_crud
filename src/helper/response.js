const responseData = (data,  statusCode = 200, message = 'Success!') => {
    return {
        status: statusCode,
        message: message,
        result: data,
    }
}

exports.responseData = responseData

