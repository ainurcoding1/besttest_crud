// Create a new Date object for the current time
const now = new Date();

// Get the individual date and time components
const year = now.getFullYear();
const month = String(now.getMonth() + 1).padStart(2, '0');
const day = String(now.getDate()).padStart(2, '0');
const hours = String(now.getHours()).padStart(2, '0');
const minutes = String(now.getMinutes()).padStart(2, '0');
const seconds = String(now.getSeconds()).padStart(2, '0');

// Create the timestamp string in the desired format
const timestamp = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;

exports.timestamp = timestamp;