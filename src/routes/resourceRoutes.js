const express = require('express');
const router = express.Router();
const ResourceController = require('../controllers/resourceController');

router.get('/', ResourceController.index);
router.get('/:id', ResourceController.show);
router.post('/', ResourceController.store);
router.put('/:id', ResourceController.update);
router.delete('/:id', ResourceController.destroy);

module.exports = router;