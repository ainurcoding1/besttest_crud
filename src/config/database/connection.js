const knex = require('knex');
const database = knex({
  client: "mysql",
  connection: {
    host: process.env.DB_HOST || "localhost",
    user: process.env.DB_USER || "root",
    password: process.env.DB_PASSWORD || "123456",
    database: process.env.DB_NAME || "ncitest",
    port: process.env.DB_PORT || "3360",
  },
});

database
  .raw("SELECT 1")
  .then(() => {
    console.log("Database connection successful!");
  })
  .catch((err) => {
    console.error("Error connecting to the database:", err);
  });

module.exports = database;


