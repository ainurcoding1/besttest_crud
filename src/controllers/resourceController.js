const { timestamp } = require('../helper/currentTImeStamp');
const { responseData } = require('../helper/response');
const Resource = require('../models/Resource');


class ResourceController {
  static async index(req, res) {
    const resources = await Resource.getAll();
    res.json(responseData(resources));
  }

  static async show(req, res) {
    const id = req.params.id;
    const resource = await Resource.getOne(id);
    if (resource.length > 0) {
      res.json(resource[0]);
    } else {
      res.status(404).send('Resource not found');
    }
  }

  static async store(req, res) {
    const { nama, gender, alamat, umur } = req.body;
    await Resource.create({ nama, gender, alamat, umur });
    res.send('Resource added successfully');
  }

  static async update(req, res) {
    const id = req.params.id;
    const updated_at = timestamp;
    const { nama, umur, gender, alamat  } = req.body;
    await Resource.update(id, { nama, umur, gender, alamat, updated_at });
    res.send('Resource updated successfully');
  }

  static async destroy(req, res) {
    const id = req.params.id;
    const data = { 
        deleted_at: timestamp,
        is_deleted: 1
    }
    await Resource.delete(id, data);
    res.send('Resource deleted successfully');
  }
}

module.exports = ResourceController;